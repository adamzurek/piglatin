<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 19. 5. 2015
 */

namespace PigLatin;


class Translator
{
    /**
     * Translates word to Pig-Latin.
     *
     * @param Word $word
     *
     * @return Word
     */
    public static function translateWord (Word $word)
    {

        // no translation for punctuation
        if ($word->isPunctuation()) {
            return $word;
        }


        if ($word->startsWithVowel()) {
            return self::translateWordVowel($word);
        }

        return self::translateWordConsonant($word);
    }

    /**
     * Translates word that starts with consonant.
     *
     * @param Word $word
     *
     * @return Word
     */
    private static function translateWordConsonant (Word $word)
    {
        list($consonants, $rest) = $word->getConsonantParts();

        return new Word($rest . '-' . $consonants . 'ay');
    }

    /**
     * Translates word that starts with vowel.
     *
     * @param Word $word
     *
     * @return Word
     */
    private static function translateWordVowel (Word $word)
    {
        return new Word($word->getWord() . '-way');
    }
}
