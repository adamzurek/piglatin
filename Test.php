<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 20. 5. 2015
 */

namespace PigLatin;

/**
 * Class Test
 *
 * Tests Pig-Latin translator and dependencies.
 *
 * @package PigTranslator
 */
class Test extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Translator
     */
    private $translator;


    /**
     * Sets up the translator and requires libraries.
     */
    public function setUp ()
    {
        require_once 'TextJoiner.php';
        require_once 'TextSplitter.php';
        require_once 'Translator.php';
        require_once 'Word.php';

        $this->translator = new Translator();
    }

    /**
     * Tests translation of single words to Pig-Latin.
     */
    public function testTranslator ()
    {
        $expectedWords = explode(' ', 'egg-way inbox-way eight-way east-bay ough-day appy-hay estion-quay ar-stay ee-thray oor-day anana-bay eagle-way');
        $testWords = explode(' ', 'egg inbox eight beast dough happy question star three door banana eagle');

        $me = $this;

        array_map(function ($word, $expectedWord) use ($me) {
            $myWord = new Word($word);
            $expected = new Word($expectedWord);
            $result = $me->translator->translateWord($myWord);

            $this->assertEquals($expected, $result);
        }, $testWords, $expectedWords);
    }

    /**
     * Tests splitter - sentences to words.
     */
    public function testSplitter ()
    {
        $testText = 'abc.def.cef,gef, hef,     jef,     def';
        $expected = array (new Word('abc'),
                           new Word('.'),
                           new Word('def'),
                           new Word('.'),
                           new Word('cef'),
                           new Word(','),
                           new Word('gef'),
                           new Word(', '),
                           new Word('hef'),
                           new Word(',     '),
                           new Word('jef'),
                           new Word(',     '),
                           new Word('def'),
                           new Word(''),    // end of string
        );


        $result = TextSplitter::split($testText);

        $this->assertEquals($expected, $result);
    }

    /**
     * Tests joiner - words to sentences.
     */
    public function testJoiner ()
    {
        $testSplitted = array (new Word('abc'),
                               new Word('.'),
                               new Word('def'),
                               new Word('.'),
                               new Word('cef'),
                               new Word(','),
                               new Word('gef'),
                               new Word(', '),
                               new Word('hef'),
                               new Word(',     '),
                               new Word('jef'),
                               new Word(',     '),
                               new Word('def'),
                               new Word(''),    // end of string
        );
        $expected = 'abc.def.cef,gef, hef,     jef,     def';

        $result = TextJoiner::join($testSplitted);

        $this->assertEquals($expected, $result);
    }

    /**
     * Tests translation of text.
     */
    public function testTextTranslation ()
    {
        $testText = 'egg inbox eight beast dough happy question star three door banana eagle';
        $expected = 'egg-way inbox-way eight-way east-bay ough-day appy-hay estion-quay ar-stay ee-thray oor-day anana-bay eagle-way';

        // split the text to words
        $splitted = TextSplitter::split($testText);

        // alias for function callback
        $me = $this;

        // translate each word
        $translatedWords = array_map(function(Word $word) use ($me) {
            return $me->translator->translateWord($word);
        }, $splitted);

        // join words to text
        $joined = TextJoiner::join($translatedWords);

        // and it's our result for test
        $result = $joined;

        $this->assertEquals($expected, $result);
    }
}
