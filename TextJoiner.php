<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 19. 5. 2015
 * Time: 22:37
 */

namespace PigLatin;


class TextJoiner
{
    /**
     * Joins words into sentence(s).
     *
     * @param Word[] $words
     *
     * @return string
     */
    public static function join (array $words)
    {
        $sentences = '';

        $sentences = implode('', $words);

        return $sentences;
    }
}
