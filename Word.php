<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 19. 5. 2015
 * Time: 22:57
 */

namespace PigLatin;


class Word
{
    /**
     * @var string
     */
    private static $vowels = 'aeio';

    /**
     * @var string
     */
    private static $punctuations = '., ';

    /**
     * @var string
     */
    private $word;


    /**
     * @param $word
     *
     * @throws \InvalidArgumentException
     */
    public function __construct ($word)
    {
        $this->word = $word;
    }

    /**
     * Splits word to consonant part and rest of the word.
     *
     * @return array(consonants, rest)
     */
    public function getConsonantParts ()
    {
        preg_match('~^([^' . self::$vowels . ']*)([a-z]*)$~', $this->word, $match);

        return array($match[1], $match[2]);
    }

    /**
     * Checks if first letter of word is vowel.
     *
     * @return bool
     */
    public function startsWithVowel ()
    {
        return isset($this->word[0]) && strpos(self::$vowels, $this->word[0]) !== false;
    }

    /**
     * Checks if word s punctuation.
     *
     * @return bool
     */
    public function isPunctuation ()
    {
        return !isset($this->word[0]) || strpos(self::$punctuations, $this->word[0]) !== false;
    }

    /**
     * @return string
     */
    public function getWord ()
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->word;
    }
}
