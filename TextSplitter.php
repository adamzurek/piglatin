<?php
/**
 * Created by PhpStorm.
 * User: Adam Žurek
 * Date: 19. 5. 2015
 */

namespace PigLatin;


class TextSplitter
{
    /**
     * Splits sentence(s) to words with punctuation/whitespace.
     *
     * @param string $text
     *
     * @return Word[]
     *
     * @throws \InvalidArgumentException
     */
    public static function split($text)
    {
        if (!is_string($text)) {
            throw new \InvalidArgumentException('Text is not string.');
        }

        $words = array();

        $count = preg_match_all('~(\w+|[,. ]+|$)~m', $text, $matches);

        if ($count == 0) {
            throw new \InvalidArgumentException('Text is not splittable.');
        }

        $words = array_map(function($word) {
            return new Word($word);
        }, $matches[1]);

        return $words;
    }
}
